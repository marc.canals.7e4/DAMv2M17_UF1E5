﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    public GameObject Player;
    public GameObject camera;
    public GameOverScreen gameOverScreen;
    public int vides, punts, oldVides, maxPoints;

    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera");
        Player = GameObject.FindGameObjectWithTag("Player");
        vides = 2;
        punts = 0;
        maxPoints = 0;

    }

    // Update is called once per frame
    void Update()
    {
        AddPoints();
        RestartLevel();      
    }
    void AddPoints() {
        if ((punts - 4 - maxPoints) < Player.GetComponent<Transform>().position.y)
        {
           punts = (int)Player.GetComponent<Transform>().position.y + 4 + maxPoints;
            
        }
    }
    void RestartLevel() {
        if (vides == 0) {
            gameOverScreen.Setup(maxPoints);
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
