﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour
{
    public Text maxPointsText;

    public void Setup(int score) {
        gameObject.SetActive(true);
        maxPointsText.text ="POINTS: "+ GameManager.Instance.maxPoints.ToString();
    }
    public void RestartButton() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
