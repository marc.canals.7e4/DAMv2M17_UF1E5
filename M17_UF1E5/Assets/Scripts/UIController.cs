﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private Text vides;
    private Text punts;
    // Start is called before the first frame update
    void Start()
    {
         vides = GameObject.Find("life_counter").GetComponent<Text>();
         punts = GameObject.Find("points_counter").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        vides.text = "LIFES: " + GameManager.Instance.vides;
        punts.text = "POINTS: " + GameManager.Instance.punts;
    }
}
