﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform originPoint;
    private Vector2 dir = new Vector2(-0.5f,0);
    public float range;
    public float movementSpeed = 3f;
    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        originPoint = this.transform.GetChild(0).transform;
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Patrolling();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player") {
            GameManager.Instance.vides -= 1;
            GameManager.Instance.Player.transform.position = new Vector3(0, -4, -1);
            GameManager.Instance.camera.transform.position = new Vector3(0, 0.4f, -10);
            GameManager.Instance.punts += 5;
            Destroy(this.gameObject);
        }
    }
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movementSpeed, rb.velocity.y);
    }
    void Patrolling()
    {
        Debug.DrawRay(originPoint.position, dir * range, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(originPoint.position, dir, range);
      
        if (hit == false) 
        {
            Flip();
           
        }

        if (hit == true) 
        {
            //this makes patrolling get bug idk why it doesn't detect the colision...
            if (hit.collider.CompareTag("LateralBorder")) 
            {
                Flip();
            }
        }
    }
    void Flip() 
    {
        movementSpeed *= -1;
        dir *= -1;
        if (sprite.flipX)
        {
            sprite.flipX = false;
        }
        else 
        {
            sprite.flipX = true;
        }
    }
    
}
