﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float jumpForce, maxJumps, speed;
    private float numJumps;
    public bool isGrounded;
    private Transform playerTF;
    private Rigidbody2D playerRB;
    private SpriteRenderer playerSR;
    private Animator playerAnim;

    // Start is called before the first frame update
    void Start()
    {
        isGrounded = true;
        playerAnim = GameManager.Instance.Player.GetComponent<Animator>();
        playerSR = GameManager.Instance.Player.GetComponent<SpriteRenderer>();
        playerTF = GameManager.Instance.Player.transform;
        playerRB = GameManager.Instance.Player.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
        Movement();
    }
    void Jump()
    {
        if (numJumps > maxJumps-1)
        {
            isGrounded = false;
        }

        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (numJumps == 0)
                {
                    playerAnim.SetBool("Jump", true);
                    numJumps++;
                    playerRB.AddForce(Vector3.up * jumpForce);
                    Debug.Log("Jump! x1");
                }
                else if (playerRB.velocity.y > 0 && playerRB.velocity.y < 4)
                {
                    numJumps++;
                    playerRB.AddForce(Vector3.up * (jumpForce - 50));
                    Debug.Log("Jump! x2");
                }

            }
            
        }
    }
    void Movement() {
        float axis = Input.GetAxis("Horizontal");
        if (axis != 0)
        {
            playerAnim.SetBool("Move", true);
            playerTF.transform.Translate(Vector3.right * axis * Time.deltaTime * speed);
            playerSR.flipX = axis < 0;
        }
        else {
            playerAnim.SetBool("Move", false);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (playerTF.transform.position.y > collision.transform.position.y)
        {
            playerAnim.SetBool("Land", true);
            playerAnim.SetBool("Jump", false);
            isGrounded = true;
            numJumps = 0;
        }
    }
}
