﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColliderDead : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.GetComponent<BoxCollider2D>().isTrigger)
        {
            if (collision.tag == "Player")
            {
                GameManager.Instance.vides -= 1;
                GameManager.Instance.maxPoints = GameManager.Instance.punts;
                GameManager.Instance.Player.transform.position = new Vector3(0, -4, -1);
                GameManager.Instance.camera.transform.position = new Vector3(0, 0.4f, -10);
            }
        }
    }
}
