﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderHealth : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (this.GetComponent<CircleCollider2D>().isTrigger)
        {
            if (collision.tag == "Player")
            {
                GameManager.Instance.vides += 1;
                Destroy(this.gameObject);
            }
        }
    }
}
