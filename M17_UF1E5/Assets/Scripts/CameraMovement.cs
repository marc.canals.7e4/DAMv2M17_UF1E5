﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Vector3 velocity = Vector3.zero;
    private Transform playerTF;
    // Start is called before the first frame update
    void Start()
    {
        playerTF = GameManager.Instance.Player.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTF.position.y > transform.position.y + 0.5f)
        {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x, playerTF.position.y + 0.5f, transform.position.z), ref velocity, 0.25f);
        }
    }
}
